# Evasive Manouvers
## A derpy game for FGJ'17

### TL;DR: what you need

- Git clone of UncoolBase
- UncoolBase/src/base symlinked to src/base
- GNU Make
- Node.js
- TypeScript 2.1.0+
- Google Closure Compiler installed on path

Optional:

- Visual Studio Code (for actually writing TypeScript)
- Simple web server for devtime (`npm -g install serve` works wonders)
- Google Chrome (for development)
- NW.JS for creating an offline package

### To compile

- Open up a terminal
- In the project folder, type `make`

This will compile the code using TypeScript Compiler and compress it using Closure,
creating the files `game.js` and `game.min.js`. The former is still readable, the latter
has had the maximum safe amount of air taken out of it and should be the one you use
in deployment.

### Continuous compilation while developing

- Open up a terminal
- In the project folder, type `make dev`

