
/// <reference path="config.ts" />
/// <reference path="base/base.ts" />
/// <reference path="game/game.ts" />
/// <reference path="game/intro.ts" />
/// <reference path="game/mainmenu.ts" />

namespace game {

    //
    // Define asset list
    //

    var assets = new base.AssetList();
    assets.setImageBaseURL("gfx/");
    assets.setSoundBaseURL("sfx/");
    assets.setMusicBaseURL("mus/");
    assets.setFontBaseUrl("font/");
    
    assets.addImage("grouplogo", "group.png");
    
    assets.addImage("intro_anim", "intro_anim.jpg");
    assets.addImage("title", "title.jpg");

    assets.addImage("player", "SpaceShipLoopsWith+-5/SpaceShipStrait.png");
    assets.addImage("player+5", "SpaceShipLoopsWith+-5/SpaceShipLoop+5.png");
    assets.addImage("player+10", "SpaceShipLoopsWith+-5/SpaceShipLoop+10.png");
    assets.addImage("player+15", "SpaceShipLoopsWith+-5/SpaceShipLoop+15.png");
    assets.addImage("player-5", "SpaceShipLoopsWith+-5/SpaceShipLoop-5.png");
    assets.addImage("player-10", "SpaceShipLoopsWith+-5/SpaceShipLoop-10.png");
    assets.addImage("player-15", "SpaceShipLoopsWith+-5/SpaceShipLoop-15.png");

    assets.addImage("enemyMine", "Enemy/Mine.png");

    assets.addImage("asteroid", "Asteroid/Asteroid.png");
    assets.addImage("missile", "Rocket/Rocket.png");

    assets.addImage("explosion", "Explosion/ExplosionSprite.png");

    assets.addFont("default", "default.xml", "default.png");
    assets.addFont("score", "score.xml", "score.png");
    assets.addFont("menu", "menu.xml", "menu.png");

    assets.addImage("routeDot", "routedot.png");
    assets.addImage("star", "star.png");

    assets.addImage("cannon0", "Turret/TurretLeft.png");
    assets.addImage("cannon1", "Turret/TurretUP.png");
    assets.addImage("cannon2", "Turret/TurretRight.png");
    assets.addImage("bullet", "bullet.png")

    assets.addImage("plasmabullet", "PlasmaBullet/PlasmaBullet.png");
    assets.addImage("plasmaparticle", "PlasmaBullet/PlasmaParticle.png");

    assets.addImage("p_smoke1", "particles/smoke1.png");

    assets.addSound("explosion", "explosion.ogg");
    assets.addSound("cannon", "cannon.ogg");
    assets.addSound("engine", "engine.ogg");

    assets.addMusic("menu", "menumusic.ogg");
    assets.addMusic("game", "gearhead.ogg");

    for(var key in config.terrainAssets) {
        for(var i = 0; i < config.terrainAssets[key]; i++) {
            assets.addImage(key + i, key + (i + 1) + ".png");
        }
    }

    var intro: Intro = null;
    var menu: MainMenu = null;
    var game: Game = null;

    export var loader = base.getLoader();

    //
    // Start built-in load and register onSuccess hook
    //

    base.getMixer().setVolume(0.8);
    base.setScreenSize(1280,720);
    base.start(assets,() => {

        intro = new Intro();
        menu = new MainMenu();
        game = new Game();

        base.requestFullscreen();

        intro.init();

    });

    export function getMenu() {
        if(menu === null) {
            console.error("Menu is not started yet!");
        }
        return menu;
    }

    export function getGame() {
        if(game === null) {
            console.error("Game is not started yet!");
        }
        return game;
    }
}
