/// <reference path="game.ts" />

namespace game {

    export class ScoreDisplay extends base.Text {

        private _score: number;
        private _digits: number;

        constructor(font: base.Font) {
            var s = '';
            var digits = config.scoredisplay.digits;
            for(var i = 0; i < digits; ++i) {
                s += '0';
            }
            super(font, s);
            this._score = 0;
            this._digits = digits;
        }

        public setScore(s: number): void {
            this._score = s;
            this.update();
        }

        private update() {
            var strscore = '' + this._score;
            for(var i = strscore.length; i < this._digits; ++i) {
                strscore = '0' + strscore;
            }
            this.setText(strscore);
        }

    }

}
