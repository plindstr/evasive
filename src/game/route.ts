/// <reference path="game.ts" />

namespace game {

    export class Route extends base.Node {

        private _dotImage: base.Image;
        private _dots: base.Sprite[];
        private _dotXPos: number[];
        private _zero: base.Vec2;

        private _periodLength: number;
        private _amplitude: number;

        private _xfrom: number;
        private _xto: number;
        private _spacing: number;

        private _timeOffset: number = 0;

        private _vtween: base.Tween = null;
        private _htween: base.Tween = null;
        private _atween: base.Tween = null;

        constructor() {
            super();
            this._dotImage = loader.getImage("routeDot");
            this._dots = [];
            this._dotXPos = [];

            var xadd = config.route.dotSpacing;
            var w = base.getScreen().getWidth();
            var dotcount = w / xadd + 1;
            var x = w * -.5;
            for(var i = 0; i < dotcount; ++i) {
                var s = new base.Sprite(this._dotImage);
                s.setPositionXY(x,0);
                this._dotXPos.push(x);
                this._dots.push(s);
                this.addChild(s);
                x += xadd;
                s.setAlpha(1.0 - Math.abs(x / (w * .5)));
                s.setDrawMode(base.DrawMode.ADDITIVE);
            }

            this._zero = new base.Vec2(config.player.positionX,0);
            this._periodLength = 1000;
            this._amplitude = 50;
            this._xfrom = w * -.5;
            this._xto = w * .5;
            this._spacing = config.route.dotSpacing;
        }

        public die(): void {

            this._vtween = new base.Tween(600, base.Easing.Quintic.Out, (bias) => {
                this.setScaleXY(this.getScale().x,1 - bias);
            }, () => {
                this._vtween = null;
            });

            this._htween = new base.Tween(1200, base.Easing.Cubic.Out, (bias) => {
                this.setScaleXY(1 - bias,this.getScale().y);
            }, () => {
                this._htween = null;
            });

            this._atween = new base.Tween(1500, base.Easing.Cubic.Out, (bias) => {
                this.setAlpha(1 - bias);
            }, () => {
                this._atween = null;
            });

            this._atween.start();
            this._vtween.start();
            this._htween.start();
        }

        public reset(): void {
            if(this._atween) this._atween.stop();
            if(this._vtween) this._vtween.stop();
            if(this._htween) this._htween.stop();

            this._atween = null;
            this._vtween = null;
            this._htween = null;

            this.setScaleXY(1);
            this.setAlpha(config.route.alpha);
        }

        public waveAt(x: number): number {
            return this.waveY(x, this._timeOffset);
        }

        public waveY(x: number, offs: number): number {
            var a = this._amplitude * .5;
            var f = (Math.PI * 2) / this._periodLength;
            return Math.sin((x - this._zero.x) * f + offs) * a;
        }

        public update(): void {

            var mpos: base.Vec2 = getGame().getInputPosition();
            mpos = stage.screenToWorld(mpos);

            var my = mpos.y * config.mouse.sensitivity_y;

            this._periodLength = config.route.period;
            this._amplitude = base.clamp(Math.abs(my),config.route.minAmplitude,config.route.maxAmplitude) * base.sign(my);

            this._timeOffset = base.wrap(this._timeOffset + base.getTimeDelta() * config.route.speed, -Math.PI, Math.PI);

            for(var i = 0, l = this._dots.length; i < l; ++i) {
                var d = this._dots[i];
                var x = this._dotXPos[i];
                var y = this.waveY(x, this._timeOffset);
                d.setPositionXY(x,y); 
            }

        }
    }
}
