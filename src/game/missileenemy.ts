/// <reference path="../base/base.ts" />
/// <reference path="../main.ts" />
/// <reference path="enemy.ts" />

namespace game {
    export class MissileEnemy extends Enemy {
        protected static _animator: base.Animator;
        protected static _random: base.Random;
        protected static _hitRadius: base.Circle;

        private _particleEmitter: base.ParticleEmitter;

        constructor() {
            if(!MissileEnemy._animator) {
                MissileEnemy._hitRadius = new base.Circle(0, 0, config.missile.hitRadius);
                MissileEnemy._random = new base.Random();
                MissileEnemy._random.init();

                var animation = new base.Animation(loader.getImage("missile"), 64, 32);
                animation.addFrameSequence(32);

                MissileEnemy._animator = new base.Animator(animation);
                MissileEnemy._animator.setLooping(true);
                MissileEnemy._animator.start();
            }

            super(MissileEnemy._animator);

            var x = Enemy._bounds.position.x + 50;
            
            /* // Disable ass missiles
            var r = Math.floor(Math.random() * 2);
            if(r == 1) {
                x = -x;
            } else {
                this.getSprite().setScaleXY(-1, 1);
            }*/

            x = -x;

            var y = Math.random() * (Enemy._bounds.size.y - 2) + 1 + Enemy._bounds.position.y;
            this.getSprite().setPositionXY(x, y);
            this._vector = new base.Vec2(MissileEnemy._random.nextInRange(config.missile.minSpeedX, config.missile.maxSpeedX) * base.sign(-x),
                MissileEnemy._random.nextInRange(config.missile.minSpeedY, config.missile.maxSpeedY) * base.sign(-y));

            // Smoke trail
            this._particleEmitter = new base.ParticleEmitter();
            this._particleEmitter.setRate(config.missile.smoke.rate);
            this._particleEmitter.setParticleScale(
                config.missile.smoke.initalScale,
                config.missile.smoke.scaleGrowMin,
                config.missile.smoke.scaleGrowMax);
            this._particleEmitter.setParticleSpread(
                config.missile.smoke.spread
            );
            this._particleEmitter.setParticleLife(
                config.missile.smoke.time_min,
                config.missile.smoke.time_max
            );
            this._particleEmitter.addSystem(smallSmokeParticles);
            this.getSprite().addChild(this._particleEmitter);
            this._particleEmitter.setPositionXY(24,0);
        }

        getHitRadius(): base.Circle {
            MissileEnemy._hitRadius.position = this.getSprite().getPosition();
            return MissileEnemy._hitRadius;
        }

        update(): void {

            super.update();
        }
    }
}
