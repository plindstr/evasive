/// <reference path="game.ts" />
/// <reference path="introanim.ts" />

namespace game {

    export class MainMenu {

        private _loop = () => this.update();
        private _stage = new base.Stage();

        private _intro: IntroAnimation;
        
        private _bg: base.Sprite;

        private _font: base.Font;
        
        private _mainMenu: base.Node;
        private _play: base.Text;
        private _story: base.Text;
        private _credits: base.Text;
        private _fullscreen: base.Text;

        private _storyMenu: base.Node;
        private _creditsMenu: base.Node;

        private _music: base.Music;

        private _mode: number = 0; // 0: main, 1: story, 2: credits

        constructor() {
            this._intro = new IntroAnimation();
            this._stage.addChild(this._intro);

            this._music = loader.getMusic("menu");
            this._music.setVolume(0.8);

            this._font = loader.getFont("menu");
            
            this._mainMenu = new base.Node();
            this._mainMenu.setPositionXY(0,-80);
            this._play = new base.Text(this._font, "Play");
            this._play.setPivotPosition(base.PivotPosition.MIDDLE);
            this._play.setPositionXY(0,100);
            this._story = new base.Text(this._font, "Story");
            this._story.setPivotPosition(base.PivotPosition.MIDDLE);
            this._story.setPositionXY(0,170);
            this._credits = new base.Text(this._font, "Credits");
            this._credits.setPivotPosition(base.PivotPosition.MIDDLE);
            this._credits.setPositionXY(0,240);
            this._fullscreen = new base.Text(this._font, "Fullscreen");
            this._fullscreen.setPivotPosition(base.PivotPosition.MIDDLE);
            this._fullscreen.setPositionXY(0,310);
   
            this._mainMenu.addChild(this._play);
            this._mainMenu.addChild(this._story);
            this._mainMenu.addChild(this._credits);
            this._mainMenu.addChild(this._fullscreen);

            var back: base.ColorLayer = null;
            var title: base.Text = null;
            var text: base.Text = null;
            var menu: base.Node = null;
            var lines: string[] = [];

            function addLine(str: string) {
                lines.push(str);
            }

            function addLinesToMenu(): void {
                var h = 35;
                var totalh = h * lines.length; 
                var y = totalh * -.5;
                for(var i = 0; i < lines.length; ++i) {
                    var t = new base.Text(loader.getFont("default"), lines[i]);
                    t.setPivotPosition(base.PivotPosition.MIDDLE);
                    t.setPositionXY(0,y);
                    menu.addChild(t);
                    y += h;
                }
                lines = [];
            }

            // Story
            menu = this._storyMenu = new base.Node();
            back = new base.ColorLayer(base.Color.BLACK);
            back.setAlpha(0.85);
            menu.addChild(back);
            
            title = new base.Text(this._font, "Story");
            title.setPivotPosition(base.PivotPosition.MIDDLE);
            title.setPositionXY(0,-295);
            menu.addChild(title);

            addLine("Evasive Action - A Heavy Metal Story");
            addLine("");
            addLine("You are the last survivor of your heavy bomber group.");
            addLine("");
            addLine("Payload and weapons are depleted.");
            addLine("The shield generator is damaged and offline.");
            addLine("");
            addLine("With your evasion systems engaged you decide");
            addLine("to make a run for it across enemy territory.");
            addLinesToMenu();

            // Credits
            menu = this._creditsMenu = new base.Node();
            back = new base.ColorLayer(base.Color.BLACK);
            back.setAlpha(0.85);
            menu.addChild(back);

            title = new base.Text(this._font, "Credits");
            title.setPivotPosition(base.PivotPosition.MIDDLE);
            title.setPositionXY(0,-295);
            menu.addChild(title);

            addLine("Programming: Patrik Lindström");
            addLine("Programming: Heikki Ohinmaa");
            addLine("Graphics: Jussi Lipponen");
            addLine("Graphics, Design: Kasper Lindström");
            addLine("Quality Assurance: Magnus Åkerfelt");
            addLine("Sound: Various (http://asoundeffect.com)");
            addLine("Music: Kevin McLeod (http://incompetech.com)");
            addLinesToMenu();
         }

        public init(): void {
            this._music.play();
            this._mode = -1;
            base.addLoop(this._loop);
            this._mainMenu.removeFromParent();
            
            this._intro.start(() => this.showMain());
        }

        public quit(): void {
            base.removeLoop(this._loop)
        }

        private gotoGame(): void {
            var fade = new base.ColorLayer(base.Color.BLACK);
            this._stage.addChild(fade);
            new base.Tween(600,base.Easing.Cubic.Out,(bias) => {
                fade.setAlpha(bias);
            }, () => {
                fade.removeFromParent();
                this.quit();
                getGame().init();
            }).start();
            this._music.stop();
        }

        private showMain(): void {
            this._stage.addChild(this._mainMenu);
            this._play.setAlpha(0.5);
            this._story.setAlpha(0.5);
            this._credits.setAlpha(0.5);
            this._fullscreen.setAlpha(0.5);
            
            new base.Tween(1250, base.Easing.Elastic.Out, (bias) => {
                this._mainMenu.setAlpha(bias);
                this._mainMenu.setScaleXY(bias);
            }, () => {
                this._mode = 0;
            }).start();
        }

        private hideMain(oncomplete = () => {}): void {
            this._mode = -1;
            new base.Tween(1250, base.Easing.Cubic.Out, (bias) => {
                this._mainMenu.setAlpha(1.0 - bias);
                this._mainMenu.setScaleXY(1.0 - bias);
            }, oncomplete).start();
        }

        private showStory(): void {
            this._mode = -1;
            this._stage.addChild(this._storyMenu);
            new base.Tween(850, base.Easing.Quadratic.In, (bias) => {
                this._storyMenu.setAlpha(bias);
                this._storyMenu.setScaleXY(bias);
            }, () => {
                this._mode = 1;
            }).start();
        }

        private hideStory(): void {
            this._mode = -1;
            new base.Tween(850, base.Easing.Cubic.Out, (bias) => {
                this._storyMenu.setAlpha(1.0 - bias);
                this._storyMenu.setScaleXY(1.0 - bias);   
            }, () => {
                this._storyMenu.removeFromParent();
                this.showMain();
            }).start();
        }

        private showCredits(): void {
            this._mode = -1;
            this._stage.addChild(this._creditsMenu);
            new base.Tween(850, base.Easing.Quadratic.In, (bias) => {
                this._creditsMenu.setAlpha(bias);
                this._creditsMenu.setScaleXY(bias);
            }, () => {
                this._mode = 2;
            }).start();
        }

        private hideCredits(): void {
            this._mode = -1;
            new base.Tween(850, base.Easing.Cubic.Out, (bias) => {
                this._creditsMenu.setAlpha(1.0 - bias);
                this._creditsMenu.setScaleXY(1.0 - bias);
            }, () => {
                this._creditsMenu.removeFromParent();
                this.showMain();
            }).start();
        }

        private getInputPos(): base.Vec2 {
            if(this._touch) return base.getTouch().getPosition();
            return base.getMouse().getPosition();
        }

        private isInputPressed(): boolean {
            return base.getMouse().isButtonPressed(0) || base.getTouch().isTapped();
        }

        private _touch: boolean = false;

        private update(): void {

            if(!this._touch) {
                var t = base.getTouch();
                if(t.isDown() || t.getDeltaX() != 0 || t.getDeltaY() != 0) {
                    this._touch = true;
                }
            } else {
                var m = base.getMouse();
                if(m.isButtonDown(0) || m.getDeltaX() != 0 || m.getDeltaY() != 0) {
                    this._touch = false;
                }
            }

            var mouse = base.getMouse();
            var keyboard = base.getKeyboard();
            var mpos = stage.screenToWorld(this.getInputPos());
            var mmy = this._mainMenu.getY();
            
            function mouseOn(text: base.Text): boolean {
                if((mpos.y - mmy > text.getY() - (text.getHeight() *.5)) && (mpos.y - mmy < text.getY() + (text.getHeight() * .75))) {
                    return true;
                }
                return false;
            }

            // Main menu
            if(this._mode === 0) {

                this._play.setAlpha(0.5);
                this._story.setAlpha(0.5);
                this._credits.setAlpha(0.5);
                this._fullscreen.setAlpha(0.5);

                var active: base.Text = null;
                if(mouseOn(this._play)) active = this._play;
                if(mouseOn(this._story)) active = this._story;
                if(mouseOn(this._credits)) active = this._credits;
                if(mouseOn(this._fullscreen)) active = this._fullscreen;

                if(active != null) {
                    active.setAlpha(1);
                }
                
                if(this.isInputPressed()) {
                    if(active == this._play) {
                        this.gotoGame();
                    } else if(active == this._story) {
                        this.hideMain(() => this.showStory());
                    } else if(active == this._credits) {
                        this.hideMain(() => this.showCredits());
                    } else if(active == this._fullscreen) {
                        base.requestFullscreen();
                    }
                }

                if(keyboard.isKeyDown(base.Keys.ESC)) {
                    window.close();
                }
            }

            // Story menu
            if(this._mode === 1) {
                if(this.isInputPressed() || keyboard.isKeyDown(base.Keys.ESC)) {
                    this.hideStory();
                }
            }

            // Credits menu
            if(this._mode === 2) {
                if(this.isInputPressed() || keyboard.isKeyDown(base.Keys.ESC)) {
                    this.hideCredits();
                }
            }

            this._stage.draw();
        }

    }

}
