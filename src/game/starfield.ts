/// <reference path="game.ts" />

namespace game {

    var random = new base.Random();

    class Star {
        public x: number;
        public y: number;
        public z: number;
        public a: number;
        public twinkle: number;
    }

    export class Starfield extends base.Node {

        private _image: base.ImageSource;
        private _speed: base.Vec2;
        private _stars: Star[];
        private _count: number;

        private _fieldWidth: number;
        private _fieldHeight: number;
        
        constructor(image: base.ImageSource, size: number = 256) {
            super();
            this._image = image;
            this._speed = new base.Vec2(-25, 0);
            this._stars = [];
            this._count = size;
            this._fieldWidth = base.getScreen().getWidth() * 1.15;
            this._fieldHeight = base.getScreen().getHeight() * 1.15;
            this.setDrawMode(base.DrawMode.ADDITIVE);

            for(var i = 0; i < size; ++i) {
                this._stars.push(new Star());
            }

            this.randomize();
        }

        public getSpeed(): base.Vec2 {
            return this._speed;
        }

        public setSpeed(dx: number, dy: number = 0): void {
            this._speed.setXY(dx,dy);
        }

        private randomize(): void {
            var x0 = this._fieldWidth * -.5;
            var x1 = this._fieldWidth * .5;
            var y0 = this._fieldHeight * -.5;
            var y1 = this._fieldHeight * .5;

            for(var i = 0, l = this._count; i < l; ++i) {
                var s = this._stars[i];
                s.x = random.nextInRange(x0,x1);
                s.y = random.nextInRange(y0,y1);
                s.z = .5 / random.nextInRange(1, 8);
                s.a = random.nextInRange(0.05,0.95);
                s.twinkle = random.nextInRange(-Math.PI,Math.PI);
            }
        }

        private update(): void {
            var delta = base.getTimeDelta();
            var xadd = this._speed.x * delta;
            var yadd = this._speed.y * delta;

            var x0 = this._fieldWidth * -.5;
            var x1 = this._fieldWidth * .5;
            var y0 = this._fieldHeight * -.5;
            var y1 = this._fieldHeight * .5;

            for(var i = 0, l = this._count; i < l; ++i) {
                var s = this._stars[i];
                s.x = base.wrap(s.x + xadd * s.z,x0,x1);
                s.y = base.wrap(s.y + yadd * s.z,y0,y1);
            }

        }

        public draw(r: base.Renderer, mtx: base.Matrix, alpha: number): void {

            var twinkle = base.getTimeCurrent() * .001;

            this.update();

            r.setMatrix(mtx);
            r.setDrawMode(this.getDrawMode());
            r.setAlpha(alpha);

            var ctx = r.getContext();
            var e = this._image.getElement();
            var sr = this._image.getSampleRect();

            var sx = sr.position.x;
            var sy = sr.position.y;
            var sw = sr.size.x;
            var sh = sr.size.y;

            var dw = sw * .5;
            var dh = sh * .5;

            for(var i = 0, l = this._count; i < l; ++i) {
                var s = this._stars[i];
                ctx.globalAlpha = s.a * .5 + Math.sin(s.twinkle + twinkle) * s.a * .25;
                ctx.drawImage(e,sx,sy,sw,sh,s.x - sw,s.y - dh,sw,sh);
            }

            ctx.globalAlpha = alpha;

        }

    }

}
