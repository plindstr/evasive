namespace game {
    export class Explosion extends Actor {
        private static animation: base.Animation;

        private _animator: base.Animator;

        constructor(x: number, y: number) {
            if(!Explosion.animation) {
                Explosion.animation = new base.Animation(loader.getImage("explosion"), 128, 128);
                Explosion.animation.addFrameSequence(128);
            }
            var animator = new base.Animator(Explosion.animation);
            animator.setAnimationSpeed(64);

            super(animator);
            this.getSprite().setPositionXY(x, y);
            this._animator = animator;

            animator.setLooping(false);
            animator.onComplete(() => {
                this._animator.reset();
                this.getSprite().setVisible(false);
                this.getSprite().removeFromParent();
            });

            game.effectLayer.addChild(this.getSprite());
            animator.start();

            this.getSprite().setScaleXY(config.explosion.scale);
        }
    }
}