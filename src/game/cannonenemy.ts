/// <reference path="game.ts" />
/// <reference path="../main.ts" />


namespace game {
    
    export class CannonEnemy extends Enemy {

        protected static _random: base.Random;
        protected static _hitRadius: base.Circle;

        private shootTime: number;
        private direction: number;
        private sound: base.Sound;

        constructor() {
            if(!CannonEnemy._random) {
                CannonEnemy._hitRadius = new base.Circle(0, 0, config.cannon.hitRadius);
                CannonEnemy._random = new base.Random();
                CannonEnemy._random.init();
            }

            var direction = Math.floor(CannonEnemy._random.nextInRange(0, 3));
            var asset = loader.getImage("cannon" + direction);
            super(asset);
            this.getSprite().setPivotPosition(base.PivotPosition.TOP_LEFT);

            this.shootTime = base.getTimeCurrent();
            this.direction = direction;

            var x = Enemy._bounds.size.x * 0.5 - 50;
            this.getSprite().setPositionXY(x, base.getScreen().getHeight() * 0.5 - asset.getHeight());
            this._vector = new base.Vec2(-CannonEnemy._random.nextInRange(config.cannon.minSpeedX, config.cannon.maxSpeedX),
                CannonEnemy._random.nextInRange(config.cannon.minSpeedY, config.cannon.maxSpeedY));

            this.sound = loader.getSound("cannon");
        }

        getHitRadius(): base.Circle {
            CannonEnemy._hitRadius.position = this.getSprite().getPosition();
            return CannonEnemy._hitRadius;
        }

        update(): void {
            super.update();

            if(getGame().isPaused()) return;

            if(this.getSprite() && base.getTimeCurrent() - this.shootTime > config.cannon.reloadSpeed) {
                this.sound.play();
                this.shootTime = base.getTimeCurrent();
                enemySpawner.spawn(new Bullet(this.getSprite().getPosition(), this._vector, config.cannon.bullet[this.direction][0]), false);
                enemySpawner.spawn(new Bullet(this.getSprite().getPosition(), this._vector, config.cannon.bullet[this.direction][1]), false);
            }
        }
    }

    class Bullet extends Enemy {
        private static animation: base.Animation;
        private static animator: base.Animator;
        private static wasinit = false;

        private static _hitRadius: base.Circle;

        private _particleEmitter: base.ParticleEmitter;

        constructor(position: base.Vec2, speed: base.Vec2, bullet) {
            if(!Bullet.wasinit) {
                Bullet.animation = new base.Animation(loader.getImage("plasmabullet"),8,20);
                Bullet.animation.addFrameSequence(32);
                Bullet.animator = new base.Animator(Bullet.animation);
                Bullet.animator.setLooping(true);
                Bullet.animator.start();
                Bullet.wasinit = true;
            }

            if(!Bullet._hitRadius) {
                Bullet._hitRadius = new base.Circle(0, 0, config.cannon.bulletHitRadius);
            }
            super(Bullet.animator);

            this.getSprite().setPosition(position);
            this.getSprite().moveXY(bullet.x, bullet.y);
            this._vector = new base.Vec2(speed.x + bullet.dx, speed.y + bullet.dy);
        
            var e = this._particleEmitter = new base.ParticleEmitter();
            e.setRate(config.plasmabullet.particles.rate);
            e.setParticleSpeed(
                config.plasmabullet.particles.speed_min,
                config.plasmabullet.particles.speed_max);
            e.setParticleAngle(90);
            e.setParticleSpread(config.plasmabullet.particles.spread);
            e.setParticleScale(
                config.plasmabullet.particles.initalScale,
                config.plasmabullet.particles.scaleGrowMin,
                config.plasmabullet.particles.scaleGrowMax);
            e.setParticleLife(
                config.plasmabullet.particles.time_min,
                config.plasmabullet.particles.time_max);

            e.addSystem(plasmaTrailParticles);

            this.getSprite().addChild(e);
        }

        update(): void {
            super.update();
        }

        getHitRadius(): base.Circle {
            Bullet._hitRadius.position = this.getSprite().getPosition();
            return Bullet._hitRadius;
        }

        destroy(): void {
            this.getSprite().setVisible(false);
            this.getSprite().removeFromParent();
            enemySpawner.remove(this, true);
        }

    }
    
}
