/// <reference path="game.ts" />

namespace game {

    export class Intro {

        private font: base.Font;
        private container: base.Node;
        private grouplogo: base.Sprite;
        private presents: base.Text;
        private stage: base.Stage;

        constructor() {

            this.stage = new base.Stage();
            this.container = new base.Node();

            this.font = loader.getFont("default");
            this.presents = new base.Text(this.font, "PRESENTS");

            this.grouplogo = new base.Sprite(loader.getImage("grouplogo"));
            this.grouplogo.setPivotPosition(base.PivotPosition.BOTTOM_CENTER);
            this.grouplogo.setPositionXY(0,-15);

            this.presents.setPivotPosition(base.PivotPosition.BOTTOM_CENTER);

            this.presents.setPositionXY(0, 75);

            this.stage.addChild(new base.ColorLayer(base.Color.BLACK));
            this.stage.addChild(this.container);
            this.container.addChild(this.grouplogo);
            this.container.addChild(this.presents);
        }

        private wait: base.Timer;

        private _loop = () => this.update();

        public init(): void {
            base.addLoop(this._loop);
            setTimeout(() => {
                new base.Tween(2500,base.Easing.Quadratic.Out,(bias) => {
                    this.container.setScaleXY(1.0 - bias);
                },() => {
                    this.quit();
                    getMenu().init();
                }).start();
            }, (1500));
        }

        public quit(): void {
            base.removeLoop(this._loop);
        }

        private update(): void {
            this.stage.draw();
        }

    }

}
