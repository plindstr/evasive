namespace game {
    export class EnemyShip extends Enemy {
        protected static _animator: base.Animator;
        protected static _random: base.Random;
        protected static _hitRadius: base.Circle;

        private _wave_low: number;
        private _wave_high: number;
        private _wave_freq: number;

        public static hasSpawned = false;

        constructor() {
            if(!EnemyShip._animator) {
                EnemyShip._hitRadius = new base.Circle(0, 0, config.enemyShip.hitRadius * config.enemyShip.scale);
                EnemyShip._random = new base.Random();
                EnemyShip._random.init();

                var animation = new base.Animation(loader.getImage("enemyMine"), 128, 128);
                animation.addFrameSequence(32);

                EnemyShip._animator = new base.Animator(animation);
                EnemyShip._animator.setLooping(true);
                EnemyShip._animator.start();
            }

            super(EnemyShip._animator);

            EnemyShip.hasSpawned = true;

            var x = -Enemy._bounds.position.x - 50;
            var y = Math.random() * (Enemy._bounds.size.y - 2) + 1 + Enemy._bounds.position.y;
            this.getSprite().setPositionXY(x, y);

            this._vector = new base.Vec2(-EnemyShip._random.nextInRange(config.enemyShip.minSpeed, config.enemyShip.maxSpeed), 0);

            this.getSprite().setScaleXY(config.enemyShip.scale);

            this._wave_low = Math.random() * -250;
            this._wave_high = Math.random() * 250;
            this._wave_freq = (Math.random() + 1) / 3;
        }

        destroy(): void {
            EnemyShip.hasSpawned = false;
            super.destroy();
        }

        getHitRadius(): base.Circle {
            EnemyShip._hitRadius.position = this.getSprite().getPosition();
            return EnemyShip._hitRadius;
        }

        update(): void {
            super.update();

            if(this.getSprite() == null) {
                return;
            }
            
            var x = this.getSprite().getX();
            this.getSprite().setPositionXY(x, base.sineWave(this._wave_low,this._wave_high,this._wave_freq));
        }
    }
}