/// <reference path="../base/base.ts" />
/// <reference path="../main.ts" />
/// <reference path="enemy.ts" />

namespace game {
    export class AsteroidEnemy extends Enemy {
        private static _animator: base.Animator;
        private static _random: base.Random;
        private static _hitRadius: base.Circle;

        constructor() {
            if(!AsteroidEnemy._animator) {
                AsteroidEnemy._hitRadius = new base.Circle(0, 0, config.asteroid.hitRadius);

                AsteroidEnemy._random = new base.Random();
                AsteroidEnemy._random.init();

                var animation = new base.Animation(loader.getImage("asteroid"), 64, 64);
                animation.addFrameSequence(32);

                AsteroidEnemy._animator = new base.Animator(animation);
                AsteroidEnemy._animator.setLooping(true);
                AsteroidEnemy._animator.start();
            }

            super(AsteroidEnemy._animator);

            var w = (Enemy._bounds.size.x - 2) * 2 + 1;
            var h = (Enemy._bounds.size.y - 2) * 2 + 1;
            var x = 0;
            var y = 0;
            var point = Math.floor(Math.random() * (w + h));
            if(point - w * 0.5 < 0) {
                x = point;
                y = Enemy._bounds.position.y + 1;
            } else if(point - w * 0.5 - h * 0.5 < 0) {
                x = Enemy._bounds.size.x * 0.5 - 1;
                y = point - w * 0.5;
            } else if(point - w - h * 0.5 < 0) {
                x = point - w * 0.5 - h * 0.5;
                y = Enemy._bounds.size.y * 0.5 - 1
            } else {
                x = Enemy._bounds.position.x + 1;
                y = point - w - h * 0.5;
            }
            this._vector = new base.Vec2(AsteroidEnemy._random.nextInRange(config.asteroid.minSpeedX, config.asteroid.maxSpeedX) * base.sign(-x),
                AsteroidEnemy._random.nextInRange(config.asteroid.minSpeedY, config.asteroid.maxSpeedY) * base.sign(-y));
            this.getSprite().setPositionXY(x, y);
        }

        getHitRadius(): base.Circle {
            AsteroidEnemy._hitRadius.position = this.getSprite().getPosition();
            return AsteroidEnemy._hitRadius;
        }

        update(): void {
            super.update();
        }
    }
}