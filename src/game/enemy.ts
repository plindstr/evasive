namespace game {
    export class Enemy extends Actor {
        protected _animator: base.Animator;
        protected _vector: base.Vec2;
        protected static _bounds: base.Rect;

        constructor(src: base.ImageSource) {
            super(src);
            if(!Enemy._bounds) {
                Enemy._bounds = new base.Rect(
                    base.getScreen().getWidth() * -0.5 - 100,
                    base.getScreen().getHeight() * -0.5 - 100,
                    base.getScreen().getWidth() + 200,
                    base.getScreen().getHeight() + 200
                );
            }
        }

        update(): void {
            var delta = base.getTimeDelta();
            var mx = this._vector.x * delta;
            var my = this._vector.y * delta;

            this.getSprite().moveXY(mx,my);

            if(this.outOfBounds()) {
                this.destroy();
            }
        }

        destroy(): void {
            enemySpawner.remove(this);
            super.destroy();
        }

        private outOfBounds(): boolean {
            var s = this.getSprite();
            return !Enemy._bounds.isPointInside(s.getPosition())
        }
    }
}