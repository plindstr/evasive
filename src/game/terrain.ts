
namespace game {
    export class Terrain extends base.Node {
        private terrain: Array<base.Sprite>;
        private terrainAssets: number;
        private random: base.Random;

        private _x0: number;
        private _y0: number;

        private speed: number;
        private prefix: string;

        private static overlap: number = 50;

        constructor(speed: number, prefix: string) {
            super();

            this.speed = speed;
            this.prefix = prefix;

            this.terrainAssets = config.terrainAssets[prefix];

            this._x0 = base.getScreen().getWidth() * -0.5 - Terrain.overlap;
            this._y0 = base.getScreen().getHeight() * 0.5;

            this.random = new base.Random();
            this.random.init();

            this.terrain = new Array();
            var last = this.spawnTerrainAssetAt(this._x0);
            while(last.getX() + last.getWidth() * 0.5 - Terrain.overlap * 2 <= -this._x0) {
                last = this.spawnTerrainAsset();
            }
        }

        private spawnTerrainAssetAt(x: number) {
            var r = this.random.nextInRange(0, this.terrainAssets);
            var asset = base.getLoader().getImage(this.prefix + Math.floor(r));
            var sprite = new base.Sprite(asset);
            sprite.setPositionXY(x + sprite.getWidth() * 0.5, this._y0 - asset.getHeight() * 0.5);
            this.addChild(sprite);
            this.terrain.push(sprite);

            return sprite;
        }

        private spawnTerrainAsset(x: number = NaN): base.Sprite {
            var last = this.terrain[this.terrain.length - 1];
            var x = last.getX() + last.getWidth() * 0.5 - Terrain.overlap;

            return this.spawnTerrainAssetAt(x);
        }

        update(): void {
            var speed = this.speed * base.getTimeDelta();

            for(var i = 0; i < this.terrain.length; i++) {
                this.terrain[i].setPositionXY(this.terrain[i].getX() - speed, this.terrain[i].getY());
            }

            if(this.terrain[0].getX() + this.terrain[0].getWidth() * 0.5 <= this._x0) {
                var last = this.terrain[this.terrain.length - 1];
                while(last.getX() + last.getWidth() * 0.5 - Terrain.overlap * 2 <= -this._x0 + 250) {
                    last = this.spawnTerrainAsset();
                }

                this.terrain[0].removeFromParent();
                this.terrain.splice(0, 1);
            }
        }
    }
}