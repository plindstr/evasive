/// <reference path="../base/base.ts" />
/// <reference path="../main.ts" />

namespace game {

    export class IntroAnimation extends base.Node {

        private _animation: base.Animation = null;
        private _animator: base.Animator = null;
        private _titlescreen: base.Image = null;
        private _callback: Function = null;
        private _sprite: base.Sprite = null;

        constructor() {
            super();

            this._titlescreen = loader.getImage("title");

            var img0 = loader.getImage("intro_anim");
            var anm = new base.Animation(img0,img0.getWidth() / 6,img0.getHeight() / 10);
            anm.addFrameSequence(60);
            this._animation = anm;
            var animator = new base.Animator(anm);
            animator.setAnimationSpeed(30);
            animator.setLooping(false);
            this._animator = animator;

            this._sprite = new base.Sprite(animator);
            this._sprite.setPivotPosition(base.PivotPosition.MIDDLE);
            this.addChild(this._sprite);

            animator.onComplete(() => {
                console.log("Intro animation complete");
                this._sprite.setImageSource(this._titlescreen);
                this._sprite.setScaleXY(1.0,1.0);
                this._sprite.setPivotPosition(base.PivotPosition.MIDDLE);
                this._sprite.setSizeXY(this._titlescreen.getWidth(),this._titlescreen.getHeight());
                if(this._callback) {
                    this._callback.call(null);
                }
            });
        }

        public start(callback: () => void): void {
            this._callback = callback;
            this._sprite.setImageSource(this._animator);
            this._sprite.setScaleXY(this._titlescreen.getWidth() / this._animator.getWidth(),this._titlescreen.getHeight() / this._animator.getHeight());
            this._sprite.setPivotPosition(base.PivotPosition.MIDDLE);
            this._animator.setFrame(0);
            this._animator.start();
        }

    }

}

