/// <reference path="enemy.ts" />
/// <reference path="asteroidenemy.ts" />
/// <reference path="missileenemy.ts" />
/// <reference path="cannonenemy.ts" />
/// <reference path="enemyship.ts" />



namespace game {
    export class EnemySpawner {
        private _maxEnemies: number = config.enemies.initialEnemies;
        private _level: number = 1;
        private _levelStart = base.getTimeCurrent();
        private _random: base.Random;
        private _overMax: number = 0;
        private _enemies = new Array<Enemy>();
        private _allowCannon: boolean = true;
        private _lastCannon: number;

        constructor() {
            this._random = new base.Random();
            this._random.init();

        }

        public remove(enemy: Enemy, wasOver: boolean = false): void {
            if(wasOver) {
                this._overMax--;
            }
            this._enemies.splice(this._enemies.indexOf(enemy), 1);
        }

        update(): void {
            for(var enemy of this._enemies) {
                enemy.update();
                if(enemy.getSprite() == null) {
                    continue;
                }

                if(!getGame().isPaused()) {
                    if(Math.abs(enemy.getSprite().getX() - player.getSprite().getX()) <= enemy.getHitRadius().radius && enemy.getHitRadius().intersects(player.getHitRadius())) {
                        enemy.destroy();
                        new Explosion(player.getSprite().getX(), player.getSprite().getY());
                        getGame().gameOver();
                    }
                }
            }

            if(base.getTimeCurrent() >= this._levelStart + config.enemies.rampUpSpeed) {
                this._levelStart = base.getTimeCurrent();
                this._level++;
                this._maxEnemies = this._maxEnemies * config.enemies.multiplier;
            }

            if(base.getTimeCurrent() - this._lastCannon > 1000) {
                this._allowCannon = true;
            }
            while(this._enemies.length < this._maxEnemies + this._overMax) {
                this.spawnEnemy();
            }
        }

        private spawnEnemy(): void {
            var r = r = this._random.next();
            var enemy: Enemy;
            if(!this._allowCannon) {
                r -= 0.1;
            }
            if(r < 0.1) {
                enemy = new AsteroidEnemy();
            } else if(r < 0.7) {
                enemy = new MissileEnemy();
            } else if(r < 0.9) {
                if(EnemyShip.hasSpawned) {
                    enemy = new AsteroidEnemy();
                } else {
                    enemy = new EnemyShip();
                }
            } else {
                enemy = new CannonEnemy();
                this._allowCannon = false;
                this._lastCannon = base.getTimeCurrent();
            }

            this.spawn(enemy);
        }

        public destroy(): void {
            for(var enemy of this._enemies) {
                enemy.destroy();
            }
            this._enemies.length = 0;
            this._enemies = null;
            EnemyShip.hasSpawned = false;
        }

        public spawn(enemy: Enemy, add: boolean = true) {
            game.actorLayer.addChild(enemy.getSprite());
            if(!add) {
                this._overMax++;
            }
            this._enemies.push(enemy);
        }
    }
}