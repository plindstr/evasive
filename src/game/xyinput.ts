/// <reference path="../base/base.ts" />
/// <reference path="../main.ts" />

namespace game {

    export class XYInput {

        private _touch: boolean = true;
        private _position: base.Vec2 = new base.Vec2();

        public reset(p: base.Vec2): void {
            this._position.set(p);
        }

        public isTouch(): boolean {
            return this._touch;
        }

        public isMouse(): boolean {
            return !this._touch;
        }

        public getPosition(): base.Vec2 {
            return this._position;
        }

        public isHeld(): boolean {
            if(this._touch) return base.getTouch().isDown();
            return base.getMouse().isButtonDown(0);
        }

        public isPressed(): boolean {
            if(this._touch) return base.getTouch().isTapped();
            return base.getMouse().isButtonPressed(0);
        }

        public update(): void {

            var t = base.getTouch();
            var m = base.getMouse();

            // determine if touch
            if(!this._touch) {
                var t = base.getTouch();
                if(t.isDown() || t.getDeltaX() != 0 || t.getDeltaY() != 0) {
                    this._touch = true;
                }
            } else {
                var m = base.getMouse();
                if(m.isButtonDown(0) || m.getDeltaX() != 0 || m.getDeltaY() != 0) {
                    this._touch = false;
                }
            }

            // track mouse absolute and touch relative
            if(this.isHeld()) {
                if(this._touch) {
                    var tdelta = t.getDelta();
                    this._position.add(tdelta);
                } else {
                    var mdelta = m.getDelta();
                    this._position.add(mdelta);
                }
            }

            var s = base.getScreen();

            this._position.clampXY(0,s.getWidth(),0,s.getHeight());

        }
        
    }

}
