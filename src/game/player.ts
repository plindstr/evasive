/// <reference path="game.ts" />
/// <reference path="actor.ts" />

namespace game {
    export class Player extends Actor {
        private static animationForward: base.Animation;
        private static animationUp5: base.Animation;
        private static animationUp10: base.Animation;
        private static animationUp15: base.Animation;
        private static animationDown5: base.Animation;
        private static animationDown10: base.Animation;
        private static animationDown15: base.Animation;

        private static hitRadius: base.Circle;

        private _animators: Object;

        private _currentAnimator: base.Animator;
        private _lastAnimationChange: number;

        private _yspeed: number;
        private _ypos: number[];

        constructor() {
            if(Player.animationForward == null) {
                Player.hitRadius = new base.Circle(0, 0, config.player.playerHitRadius);

                Player.animationForward = new base.Animation(loader.getImage("player"), 256, 128);
                Player.animationForward.addFrameSequence(32);

                Player.animationUp5 = new base.Animation(loader.getImage("player+5"), 256, 128);
                Player.animationUp5.addFrameSequence(32);

                Player.animationUp10 = new base.Animation(loader.getImage("player+10"), 256, 128);
                Player.animationUp10.addFrameSequence(32);

                Player.animationUp15 = new base.Animation(loader.getImage("player+15"), 256, 128);
                Player.animationUp15.addFrameSequence(32);

                Player.animationDown5 = new base.Animation(loader.getImage("player-5"), 256, 128);
                Player.animationDown5.addFrameSequence(32);

                Player.animationDown10 = new base.Animation(loader.getImage("player-10"), 256, 128);
                Player.animationDown10.addFrameSequence(32);

                Player.animationDown15 = new base.Animation(loader.getImage("player-15"), 256, 128);
                Player.animationDown15.addFrameSequence(32);
            }

            var animator = new base.Animator(Player.animationForward);
            super(animator);

            this._lastAnimationChange = 0;
            this._animators = {
                forward: null,
                up5: null,
                up10: null,
                up15: null,
                down5: null,
                down10: null,
                down15: null
            };
            this._currentAnimator = this._animators["forward"] = animator;
            this._currentAnimator.setLooping(true);
            this._currentAnimator.start();

            this._animators["up5"] = new base.Animator(Player.animationUp5);
            this._animators["up5"].setLooping(true);

            this._animators["up10"] = new base.Animator(Player.animationUp10);
            this._animators["up10"].setLooping(true);

            this._animators["up15"] = new base.Animator(Player.animationUp15);
            this._animators["up15"].setLooping(true);

            this._animators["down5"] = new base.Animator(Player.animationDown5);
            this._animators["down5"].setLooping(true);

            this._animators["down10"] = new base.Animator(Player.animationDown10);
            this._animators["down10"].setLooping(true);

            this._animators["down15"] = new base.Animator(Player.animationDown15);
            this._animators["down15"].setLooping(true);

            this.getSprite().setPositionXY(config.player.positionX,0);
            this.getSprite().setScaleXY(-1, 1);
        
            this._yspeed = 0;
            this._ypos = [0, 0, 0, 0, 0];    
        }

        getHitRadius(): base.Circle {
            Player.hitRadius.position = this.getSprite().getPosition();
            return Player.hitRadius;
        }

        public update(): void {

            var ythresh = 5;

            var mpos: base.Vec2 = getGame().getInputPosition(); 
            mpos = stage.screenToWorld(mpos);

            var delta = base.getTimeDelta();
            var s = this.getSprite();
            var x = base.clamp(mpos.x * config.mouse.sensitivity_x,config.player.min_x,config.player.max_x);
            var y = route.waveAt(x);

            // Calculate smoothed Y speed;
            this._ypos.pop();
            this._ypos.push(y - this._ypos[0]);
            this._yspeed = 0;
            for(var i = 0; i < 5; ++i) {
                this._yspeed += this._ypos[i];
            }
            this._yspeed /= 5;
            
            // Change animators
            if(base.getTimeCurrent() - this._lastAnimationChange > config.player.yawChangeThreshold) {
                if(Math.abs(this._yspeed) > config.yawThreshold) {
                    this._lastAnimationChange = base.getTimeCurrent();

                    var sign = base.sign(this._yspeed);
                    var diff = Math.abs(this._yspeed);

                    var num = 5;
                    if(diff > config.yawThreshold * 3) {
                        num = 15;
                    } else {
                        num = 10;
                    }
                    //this._currentAnimator.stop();

                    if(sign < 0) {
                        var dir = "up";
                    } else {
                        var dir = "down"
                    }
                    if(this._currentAnimator != this._animators[dir + num]) {
                        this.changeAnimator(this._animators[dir + num]);
                    }
                } else if(this._currentAnimator != this._animators["forward"]) {
                    this.changeAnimator(this._animators["forward"]);
                }
            }
            
            s.setPositionXY(x,y);
        }

        private changeAnimator(animator: base.Animator): void {
            var frame = this._currentAnimator.getFrame();
            this._currentAnimator.stop();
            this._currentAnimator = animator;
            this.getSprite().setImageSource(this._currentAnimator);
            this._currentAnimator.setFrame(frame);
            this._currentAnimator.start();
        }
    }
}