/// <reference path="../main.ts" />
/// <reference path="player.ts" />
/// <reference path="route.ts" />
/// <reference path="terrain.ts" />
/// <reference path="enemyspawner.ts" />
/// <reference path="explosion.ts" />
/// <reference path="starfield.ts" />
/// <reference path="menu.ts" />
/// <reference path="gameovermenu.ts" />
/// <reference path="scoredisplay.ts" />
/// <reference path="xyinput.ts" />


namespace game {

    export var stage: base.Stage;
    export var bgLayer: base.Node;
    export var fgLayer: base.Node;
    export var uiLayer: base.Node;
    export var actorLayer: base.Node;
    export var effectLayer: base.Node;
    export var bulletLayer: base.Node;
    export var enemySpawner: game.EnemySpawner;
    export var player: game.Player;
    export var route: game.Route;
    export var xyinput: game.XYInput = new game.XYInput();

    export var starfield: game.Starfield;

    export var smallSmokeParticles: base.ParticleSystem
    export var plasmaTrailParticles: base.ParticleSystem;

    export class Game {
        public player: Player;
        private terrain: Terrain;
        private terrain2: Terrain;
        private menu: Menu;

        private paused: boolean = false;
        private startTime: number;
        private score: number = 0;
        private highscore: number = 0;
     
        private scoreCounter: ScoreDisplay;

        private explosionSound: base.Sound;
        private engineSound: base.Sound;

        private music: base.Music;

        constructor() {
            var ls = window.localStorage;
            if(ls) {
                var score = ls.getItem("highscore");
                try {
                    this.highscore = parseInt(score);
                    if(isNaN(this.highscore)) {
                        this.highscore = 0;
                    }
                } catch(ex) {
                    this.highscore = 0;
                }
            }

            stage = new base.Stage();

            bgLayer = new base.Node();
            fgLayer = new base.Node();
            uiLayer = new base.Node();

            actorLayer = new base.Node();
            effectLayer = new base.Node();
            bulletLayer = new base.Node();

            stage.addChild(new base.ColorLayer(base.Color.BLACK)); // clear layer
            stage.addChild(bgLayer);
            stage.addChild(actorLayer);
            stage.addChild(bulletLayer);
            stage.addChild(effectLayer);
            stage.addChild(fgLayer);
            stage.addChild(uiLayer);

            // add starfield
            starfield = new Starfield(loader.getImage("star"), config.starfield.numstars);
            starfield.setSpeed(config.starfield.speed_x,config.starfield.speed_y);
            starfield.setAlpha(config.starfield.alpha);
            bgLayer.addChild(starfield);

            // create particle systems
            smallSmokeParticles = new base.ParticleSystem(loader.getImage("p_smoke1"));
            smallSmokeParticles.setDrawMode(base.DrawMode.ALPHA);
            smallSmokeParticles.setGravity(new base.Vec2(config.smoke.gravity_x,config.smoke.gravity_y));
            smallSmokeParticles.setScroll(config.particleScroll);

            plasmaTrailParticles = new base.ParticleSystem(loader.getImage("plasmaparticle"));
            plasmaTrailParticles.setDrawMode(base.DrawMode.ADDITIVE);
            plasmaTrailParticles.setGravity(new base.Vec2(config.plasmabullet.particles.gravity_x,config.plasmabullet.particles.gravity_y));
            plasmaTrailParticles.setScroll(config.particleScroll);

            this.terrain = new Terrain(config.terrainSpeed * 0.5, "BackGround");
            this.terrain.setPositionXY(0, -30);
            bgLayer.addChild(this.terrain);

            this.terrain2 = new Terrain(config.terrainSpeed, "FowardGround");
            bgLayer.addChild(this.terrain2);

            route = new Route();
            route.setAlpha(config.route.alpha);
            bgLayer.addChild(route);

            this.scoreCounter = new ScoreDisplay(loader.getFont("score"));
            this.scoreCounter.setPivotPosition(base.PivotPosition.TOP_CENTER);
            this.scoreCounter.setPositionXY(0,base.getScreen().getHeight() * -.5 + 5);
            this.scoreCounter.setScaleXY(config.scoredisplay.scale);
            uiLayer.addChild(this.scoreCounter);

            this.explosionSound = loader.getSound("explosion");

            this.engineSound = loader.getSound("engine");
            this.engineSound.getWaud().loop(true);
            this.engineSound.stop();

            this.music = loader.getMusic("game");
            this.music.setVolume(0.8);            
        }

        private _loop = () => this.update();

        public getInputPosition(): base.Vec2 {
            return xyinput.getPosition();
        }

        public getInputHeld(): boolean {
            return xyinput.isHeld();
        }

        public getInputPressed(): boolean {
            return xyinput.isPressed();
        }

        public isTouch(): boolean {
            return xyinput.isTouch();
        }

        public init(): void {
            this.locked = false;
            base.addLoop(this._loop);
            this.music.play();
            this.start();
        }

        public quit(): void {
            this.music.stop();
            base.getMixer().stopAllSounds();
            base.removeLoop(this._loop);
        }

        public start(): void {
            xyinput.reset(new base.Vec2(1280 * 0.25, 720 * 0.5));

            actorLayer.clearChildren();
            effectLayer.clearChildren();
            bulletLayer.clearChildren();

            effectLayer.addChild(plasmaTrailParticles);
            effectLayer.addChild(smallSmokeParticles);

            if(player) {
                player.destroy();
            }
            player = new Player();
            this.engineSound.play();
            actorLayer.addChild(player.getSprite());

            route.reset();

            requestAnimationFrame(() => {
                this.engineSound.play();
            });

            if(enemySpawner) {
                enemySpawner.destroy();
            }
            enemySpawner = new EnemySpawner();

            this.startTime = base.getTimeCurrent();
            this.setPaused(false);            
        }

        public setPaused(paused: boolean): void {
            if(paused) {
                this.score += this.getScore();
            } else {
                this.startTime = base.getTimeCurrent();
            }
            this.paused = paused;
        }

        public isPaused(): boolean {
            return this.paused;
        }

        public gameOver(): void {
            this.engineSound.stop();
            this.explosionSound.play();
            route.die();
            player.getSprite().setVisible(false);

            var g = this.menu = new GameOverMenu();
            uiLayer.addChild(this.menu);
            new base.Tween(3500,base.Easing.Elastic.Out,(bias) => {
                g.setScaleXY(bias);
            }).start();
            new base.Tween(3500,base.Easing.Cubic.Out,(bias) => {
                g.setAlpha(bias);
            }).start();

            this.setPaused(true);
        }

        public updateHighScore(currentScore: number): number {
            this.highscore = Math.max(this.highscore,currentScore);

            var ls = window.localStorage;
            if(ls) {
                ls.setItem("highscore","" + this.highscore);
            }

            return this.highscore;
        }

        public getScore(): number {
            return Math.floor(base.getTimeCurrent() - this.startTime);
        }

        public gotoMenu() {
            var fade = new base.ColorLayer(base.Color.BLACK);
            stage.addChild(fade);
            new base.Tween(500,base.Easing.Cubic.Out,(bias) => {
                fade.setAlpha(bias);
            }, () => {
                this.menu.removeFromParent();
                this.start();
                fade.removeFromParent();
                this.quit();
                getMenu().init();
            }).start(); 
            this.locked = true;
        }

        private locked: boolean = false;

        // Main loop
        public update(): void {

            xyinput.update();

            enemySpawner.update();
            this.terrain.update();
            this.terrain2.update();

            if(this.paused) {
                stage.draw();                
                if(!this.locked) {
                    if(this.menu) {
                        this.menu.update();
                    }
                }
                return;
            }

            route.update();
            player.update();
            
            this.scoreCounter.setScore(this.getScore());

            stage.draw();

        }

    }

}
