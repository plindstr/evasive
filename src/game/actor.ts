/// <reference path="game.ts" />

namespace game {

    export class Actor {

        private _sprite: base.Sprite;
        private _alive: boolean;

        constructor(image: base.ImageSource) {
            this._sprite = new base.Sprite(image);
            this._alive = false;
        }

        public destroy(): void {
            this._sprite.setVisible(false);
            this._sprite.removeFromParent();
            this._sprite = null;
            this._alive = false;
        }

        getHitRadius(): base.Circle {
            return new base.Circle();
        }

        public getSprite(): base.Sprite {
            return this._sprite;
        }
    }

}
