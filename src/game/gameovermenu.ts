/// <reference path="../base/base.ts" />
/// <reference path="../main.ts" />
/// <reference path="menu.ts" />

namespace game {
    export class GameOverMenu extends Menu {
        constructor() {
            super();

            this.addChild(new base.ColorLayer(base.Color.BLACK));

            var font = loader.getFont("menu");
            
            var text = new base.Text(font, "Game over");
            text.setPivotPosition(base.PivotPosition.MIDDLE);
            text.setPositionXY(0, -260);
            this.addChild(text);

            text = new base.Text(font, "High score: " + game.getGame().updateHighScore(game.getGame().getScore()));
            text.setPivotPosition(base.PivotPosition.MIDDLE);
            text.setPositionXY(0, -120);
            this.addChild(text);

            text = new base.Text(font, "Your score: " + game.getGame().getScore());
            text.setPivotPosition(base.PivotPosition.MIDDLE);
            text.setPositionXY(0, -20);
            this.addChild(text);

            text = new base.Text(font, "Retry");
            text.setPivotPosition(base.PivotPosition.MIDDLE);
            text.setPositionXY(-350, 200);
            this.addChild(text);

            text = new base.Text(font, "Exit");
            text.setPivotPosition(base.PivotPosition.MIDDLE);
            text.setPositionXY(350,200);
            this.addChild(text);
        }

        public update(): void {
            
            if(getGame().getInputPressed()) {
                
                var clickpos: base.Vec2;
                if(getGame().isTouch()) {
                    clickpos = base.getTouch().getPosition();
                } else {
                    clickpos = base.getMouse().getPosition();
                }
                clickpos = stage.screenToWorld(clickpos);

                if(clickpos.y > 0) {

                    if(clickpos.x < 0) {
                        getGame().start();
                        this.removeFromParent();
                    } else {
                        getGame().gotoMenu();        
                    }

                }

            }

            if(base.getKeyboard().isKeyPressed(base.Keys.ESC)) {
                getGame().gotoMenu();
            }
        }
    }
}
