/// <reference path="base/base.ts" />

namespace config {

    export var terrainSpeed = 230;
    export var yawThreshold = 5;
    export var particleScroll = new base.Vec2(-terrainSpeed,0);
    

    export var scoredisplay = {
        scale: 0.55,
        digits: 8
    };

    export var route = {
        dotSpacing: 12,
        minFreq: 175,
        period: 900,
        minAmplitude: 0,
        maxAmplitude: 460,
        speed: 1.23,
        alpha: 0.77
    };

    export var cursor = {
        radius: 50,
        color: new base.Color(64,256,72,192),
    };

    export var starfield = {
        speed_x: -75,
        speed_y: 0,
        numstars: 63,
        alpha: 0.8
    };

    export var mouse = {
        sensitivity_x: 0.54,
        sensitivity_y: 1.55,
    };

    export var player = {
        positionX: 1280 * -0.25,
        min_x: 1280 * -0.45,
        max_x: 1280 * 0.45,
        playerHitRadius: 36,
        yawChangeThreshold: 200
    };

    export var terrainAssets = {
        BackGround: 3,
        FowardGround: 1
    };

    export var enemies = {
        initialEnemies: 3,
        rampUpSpeed: 10000,
        multiplier: 1.1
    };

    export var explosion = {
        scale: 2.75,
    };

    export var asteroid = {
        minSpeedX: 130,
        maxSpeedX: 200,
        minSpeedY: 160,
        maxSpeedY: 250,
        hitRadius: 32
    };

    export var smoke = {
        gravity_x: 5,
        gravity_y: -42
    };

    export var missile = {
        minSpeedX: 360,
        maxSpeedX: 425,
        minSpeedY: -5,
        maxSpeedY: 5,
        hitRadius: 10,

        smoke: {
            rate: 50,
            initalScale: 0.5,
            scaleGrowMin: 0.1,
            scaleGrowMax: 1.0,
            spread: 20,
            time_min: 200,
            time_max: 600
        }
    };

    export var enemyShip = {
        minSpeed: 215,
        maxSpeed: 275,
        hitRadius: 42,
        scale: 0.5
    };

    export var plasmabullet = {
        particles: {
            rate: 100,
            initalScale: 1,
            scaleGrowMin: 1.5,
            scaleGrowMax: 2.0,
            speed_min: 2,
            speed_max: 50,
            spread: 50,
            time_min: 150,
            time_max: 400,
            gravity_x: 0,
            gravity_y: 0,
        },
        bulletSpeed: 250
    };

    export var cannon = {
        minSpeedX: terrainSpeed,
        maxSpeedX: terrainSpeed,
        minSpeedY: 0,
        maxSpeedY: 0,
        hitRadius: 0,
        reloadSpeed: 1000,
        bulletHitRadius: 6,

        bullet: [
            [ // left
                {
                    x: 14,
                    y: 36,
                    dx: -plasmabullet.bulletSpeed,
                    dy: -plasmabullet.bulletSpeed
                },
                {
                    x: 24,
                    y: 26,
                    dx: -plasmabullet.bulletSpeed,
                    dy: -plasmabullet.bulletSpeed
                }
            ],
            [ // up
                {
                    x: 54,
                    y: 16,
                    dx: 0,
                    dy: -plasmabullet.bulletSpeed
                },
                {
                    x: 68,
                    y: 16,
                    dx: 0,
                    dy: -plasmabullet.bulletSpeed
                }
            ],
            [ // right
                {
                    x: 97,
                    y: 27,
                    dx: plasmabullet.bulletSpeed,
                    dy: -plasmabullet.bulletSpeed
                },
                {
                    x: 108,
                    y: 36,
                    dx: plasmabullet.bulletSpeed,
                    dy: -plasmabullet.bulletSpeed
                }
            ]
        ]
    };
}